ARG GODOT_VERSION
ARG GODOT_RELEASE_TYPE="stable"
ARG GODOT_PLATFORM="linux.x86_64"


# Downloaded files in this stage should be cached across image builds
FROM alpine:3.17.2 AS resources

ARG GODOT_VERSION
ARG GODOT_RELEASE_TYPE
ARG GODOT_PLATFORM

RUN test -n "$GODOT_VERSION" || { echo "GODOT_VERSION must be set" && false; }

RUN echo "Installing tools" \
 && apk add --no-cache \
    curl \
    unzip \
 && echo "Installed tools"

ARG BASE_GODOT_URL="https://github.com/godotengine/godot/releases/download"
ARG BASE_GODOT_VERSION_URL="${BASE_GODOT_URL}/${GODOT_VERSION}-${GODOT_RELEASE_TYPE}"
ARG BASE_GODOT_FILENAME="Godot_v${GODOT_VERSION}-${GODOT_RELEASE_TYPE}"

WORKDIR /downloads

# File: /downloads/godot
RUN echo "Downloading Godot" \
 && curl -L -O "${BASE_GODOT_VERSION_URL}/${BASE_GODOT_FILENAME}_${GODOT_PLATFORM}.zip" \
 && unzip "${BASE_GODOT_FILENAME}_${GODOT_PLATFORM}.zip" \
 && rm -f "${BASE_GODOT_FILENAME}_${GODOT_PLATFORM}.zip" \
 && mv "${BASE_GODOT_FILENAME}_${GODOT_PLATFORM}" "godot" \
 && ls -alh "godot" \
 && echo "Downloaded Godot"

# Directory: /downloads/templates/
RUN echo "Downloading Godot export templates" \
 && curl -L -O "${BASE_GODOT_VERSION_URL}/${BASE_GODOT_FILENAME}_export_templates.tpz" \
 && unzip "${BASE_GODOT_FILENAME}_export_templates.tpz" \
 && rm -f "${BASE_GODOT_FILENAME}_export_templates.tpz" \
 && ls -alh "templates/" \
 && echo "Downloaded Godot export templates"

ARG TEMPLATES_GLOB

# Since this RUN layer is not included in the final image,
# we can separate the download and removal into their own RUN command
# without worrying about the image size (since OverlayFS is used)
# Note that Godot does not provide export templates individually
# (https://github.com/godotengine/godot-proposals/issues/647)
RUN echo "Removing unneeded Godot export templates" \
 && find "templates/" \
    -type f \
    -not -name "$TEMPLATES_GLOB" \
    -delete -print \
 && ls -alh "templates/" \
 && echo "Removed unneeded Godot export templates"


FROM ubuntu:22.04

ARG GODOT_VERSION
ARG GODOT_RELEASE_TYPE
ARG GODOT_PLATFORM

LABEL "org.opencontainers.image.authors"="Jonston Chan (jonstonchan)"
LABEL "org.opencontainers.image.documentation"="https://hub.docker.com/r/jonstonchan/godot"
LABEL "org.opencontainers.image.url"="https://gitlab.com/docker-image-builder/godot"
LABEL "org.opencontainers.image.source"="https://gitlab.com/docker-image-builder/godot"
LABEL "org.opencontainers.image.version"="${GODOT_VERSION}-${GODOT_RELEASE_TYPE}-${GODOT_PLATFORM}"

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC

RUN apt-get update \
 && apt-get install -y \
    libfontconfig1 \
 && rm -rf /var/lib/apt/lists/*

ENV HOME="/home/root"

ARG GODOT_BIN_DIR="${HOME}/.local/bin"
ENV PATH="${PATH}:${GODOT_BIN_DIR}"

COPY --from=resources "/downloads/godot" "${GODOT_BIN_DIR}/"

ARG GODOT_TEMPLATE_DIR="${HOME}/.local/share/godot/export_templates/${GODOT_VERSION}.${GODOT_RELEASE_TYPE}"

COPY --from=resources "/downloads/templates/" "${GODOT_TEMPLATE_DIR}/"

RUN echo "Generating default config file" \
 && godot --headless --editor --quit \
 && echo "Generated default config file" \
