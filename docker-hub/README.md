# Contributing

- [GitLab](https://gitlab.com/docker-image-builder/godot)


# Purpose

[Godot does not provide export templates individually](https://github.com/godotengine/godot-proposals/issues/647),
so the whole 750+ MB zip file needs to be downloaded.

This Docker repository provides Godot (headless)
with individual access (by platform) to export templates.
It is intended for CI usage.


# Tags

## Examples

- `jonstonchan/godot:4.0.1-all`
- `jonstonchan/godot:4.0.1-android`
- `jonstonchan/godot:4.0.1-linux`
- `jonstonchan/godot:4.0.1-none`
- `jonstonchan/godot:4.0.1-web`
- `jonstonchan/godot:4.0.1-windows`

Namespace included in examples for convenience.


## Template

- `<version>-<export template type>`
    - `version`
        - `4.0.1`
    - `export template type`
        - `all`
            - Does not include Java and Android SDK
        - `android`
            - Includes Java and Android SDK
        - `linux`
        - `none`
            - Can be used to export `.pck`
        - `web`
        - `windows`

Java and Android SDK not included unless explicitly specified.


# Usage

## GitLab CI

### Minimal version

**`.gitlab-ci.yml`**

```yml
export web pck:
  image: jonstonchan/godot:4.0.1-none
  script:
    - godot --quiet --headless --export-pack Web demo-web.pck
```

Note: preset name `Web` is from `export_presets.cfg`
and should be modified depending on your project's export presets.


### Complete version

**`.gitlab-ci.yml`**

```yml
stages:
  - build
  - deploy

variables:
  BUILD_ALL:
    description: "Should binaries for all architectures be built? Overrides individual BUILD_* options. 'true' or 'false'"
    value: "false"
  BUILD_WEB:
    description: "Should binaries for JavaScript be built? 'true' or 'false'"
    value: "true"
  BUILD_LINUX:
    description: "Should binaries for Linux be built? 'true' or 'false'"
    value: "false"
  BUILD_WINDOWS:
    description: "Should binaries for Windows be built? 'true' or 'false'"
    value: "false"
  BUILD_ANDROID:
    description: "Should binaries for Android be built? 'true' or 'false'"
    value: "false"
  BUILDS_DIRECTORY: $CI_PROJECT_DIR/builds

.build_template:
  stage: build
  variables:
    DEFAULT_GODOT_OPTIONS: --quiet --headless --path src/
  before_script:
    - mkdir -p "$BUILDS_DIRECTORY"
    # Generates ".godot/imported/*"
    - godot $DEFAULT_GODOT_OPTIONS --editor --quit
  script:
    - godot $DEFAULT_GODOT_OPTIONS --export-release "$PRESET_NAME" "$BUILDS_DIRECTORY/$BUILD_NAME"
  artifacts:
    paths:
      - $BUILDS_DIRECTORY

web:
  extends: .build_template
  image: jonstonchan/godot:4.0.1-web
  variables:
    PRESET_NAME: "Web"
    BUILD_NAME: "index.html"
  rules:
    - if: $BUILD_ALL == "true" || $BUILD_WEB == "true"
      when: always

linux:
  extends: .build_template
  image: jonstonchan/godot:4.0.1-linux
  variables:
    PRESET_NAME: "Linux"
    BUILD_NAME: "demo"
  rules:
    - if: $BUILD_ALL == "true" || $BUILD_LINUX == "true"
      when: always

windows:
  extends: .build_template
  image: jonstonchan/godot:4.0.1-windows
  variables:
    PRESET_NAME: "Windows"
    BUILD_NAME: "demo.exe"
  rules:
    - if: $BUILD_ALL == "true" || $BUILD_WINDOWS == "true"
      when: always

android:
  extends: .build_template
  image: jonstonchan/godot:4.0.1-android
  variables:
    PRESET_NAME: "Android (debug)"
    BUILD_NAME: "demo.apk"
  script:
    - godot $DEFAULT_GODOT_OPTIONS --export-debug "$PRESET_NAME" "$BUILDS_DIRECTORY/$BUILD_NAME"
  rules:
    - if: $BUILD_ALL == "true" || $BUILD_ANDROID == "true"
      when: always

pages:
  stage: deploy
  image: alpine:3.17.2
  needs:
    - web
  script:
    - mv "$BUILDS_DIRECTORY" public
    - find
        public/
        -type f
        -exec gzip -9 -k {} +
  artifacts:
    paths:
      - public/
  rules:
    - if: $BUILD_ALL == "true" || $BUILD_WEB == "true"
      when: on_success
```
