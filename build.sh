#!/usr/bin/env bash

set -e

if [ "$DOCKER_REGISTRY" = "$CI_REGISTRY" ]; then
	is_using_gitlab_registry=true
else
	is_using_gitlab_registry=false
fi

# Main development branch should always use GitLab registry
if [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]; then
	docker login \
		-u "$CI_REGISTRY_USER" \
		-p "$CI_REGISTRY_PASSWORD" \
		"$CI_REGISTRY"

	is_using_gitlab_registry=true
else
	docker login \
		-u "$DOCKER_USERNAME" \
		-p "$DOCKER_AUTH_TOKEN" \
		"$DOCKER_REGISTRY"

	is_using_gitlab_registry=false
fi

if [ "$is_using_gitlab_registry" = true ]; then
	DOCKER_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE}/${DOCKER_NAMESPACE}"
else
	DOCKER_REGISTRY_IMAGE="${DOCKER_REGISTRY}/${DOCKER_NAMESPACE}"
fi

echo "INFO: DOCKER_REGISTRY_IMAGE is '$DOCKER_REGISTRY_IMAGE'"


function build_android_image() {
	# Android needs to be built separately because:
	#   1. image containing only Android export templates does not need to be pushed
	#   2. additional dependencies (Java and Android SDK) and environment variables are required
	templates_name="android"
	templates_glob="android*"

	echo "INFO: building '$templates_name' (glob: '$templates_glob')"

	docker_image_tag="${DOCKER_REGISTRY_IMAGE}:${GODOT_VERSION}-${templates_name}"
	docker_image_tag_templates_only="${DOCKER_REGISTRY_IMAGE}:${GODOT_VERSION}-${templates_name}-templates-only"

	docker build \
		--build-arg "GODOT_VERSION=${GODOT_VERSION}" \
		--build-arg "TEMPLATES_GLOB=${templates_glob}" \
		-t "$docker_image_tag_templates_only" . \
	2>&1 | tee "android-templates-only.log"

	docker build \
		-f "Dockerfile.android" \
		--build-arg "BASE_IMAGE=${docker_image_tag_templates_only}" \
		-t "$docker_image_tag" . \
	2>&1 | tee "android.log"

	docker push "$docker_image_tag"
}

function build_general_images() {
	# Key is tag name, value is template glob
	declare -A template_globs
	template_globs["all"]="*"
	template_globs["none"]=""

	template_globs["linux"]="linux*"
	template_globs["web"]="web*"
	template_globs["windows"]="windows*"

	for templates_name in ${!template_globs[@]}; do
		templates_glob="${template_globs[${templates_name}]}"

		echo "INFO: building '$templates_name' (glob: '$templates_glob')"

		docker_image_tag="${DOCKER_REGISTRY_IMAGE}:${GODOT_VERSION}-${templates_name}"

		docker build \
			--build-arg "GODOT_VERSION=${GODOT_VERSION}" \
			--build-arg "TEMPLATES_GLOB=${templates_glob}" \
			-t "$docker_image_tag" . \
		2>&1 | tee "${templates_name}.log"

		docker push "$docker_image_tag"
	done
}

# Android image is built first since it extends the general image commands
# and allows detecting issues earlier
build_android_image
build_general_images
